/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ryang.tools;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author randy
 * https://examples.javacodegeeks.com/core-java/util/concurrent/java-8-concurrency-tutorial/
 * http://www.slideshare.net/jpaumard/asynchronous-api-in-java8-how-to-use-completablefuture
 */
public class AsyncFuzzySearch {
    Logger log = Logger.getLogger(AsyncFuzzySearch.class.getName());
    private List<String> dictionary;

    public static void main(String[] args) {
        AsyncFuzzySearch asyncFuzzySearch = new AsyncFuzzySearch();
        asyncFuzzySearch.run();
    }
    
    public void run() {
        List<String> wordList = new ArrayList<>();
        List<Future> futureList = new ArrayList<>();
        
        dictionary = new ArrayList<>();
        dictionary.add("sol");
        dictionary.add("sor");
        dictionary.add("solra");
        dictionary.add("solr");
        dictionary.add("asolr");
        dictionary.add("asyn");
        dictionary.add("asynch");
        dictionary.add("async");

        wordList.add("slra");
        wordList.add("solr");
        wordList.add("async");
        
        for (String string : wordList) {
            CompletableFuture data = CompletableFuture.runAsync(() -> this.asyncMethod(string));
            futureList.add(data);
        }
        
        try {
            for (Future future : futureList) {
                future.get(2, TimeUnit.MINUTES);
            }
        } catch (TimeoutException | InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

    }
    
    public void asyncMethod(String candidate) {
        int highestScore = 0;
        int currentScore = 0;
        String bestMatch = "";
        
        for (String word : dictionary) {
            currentScore = StringUtils.getFuzzyDistance(candidate, word, Locale.ENGLISH);
            
            if (candidate.equals(word)) {
                currentScore = 1000;
            }
            
            if (currentScore > highestScore) {
                highestScore = currentScore;
                bestMatch = word;
            }
        }
        System.out.println("Candidate: " + candidate + " - Best: " + bestMatch + " - Score: " + highestScore);
    }
}
